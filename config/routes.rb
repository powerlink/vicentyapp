Rails.application.routes.draw do

  get 'sessions/create'

	devise_for :users,skip: :sessions , :controllers => { omniauth_callbacks: 'omniauth_callbacks' }
	as :user do
		post '/login' => 'sessions#create'
	end

	root :to => 'home#index'

	resources :contacts, :except => [:new]
	resources :deals, :except => [:new]
	resources :contacts_phones_emails, :except => [:new]
end


