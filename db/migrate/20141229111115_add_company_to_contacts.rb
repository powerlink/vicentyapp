class AddCompanyToContacts < ActiveRecord::Migration
	def up
		add_column :contacts, :company, :string
	end	

	def down
		remove_column :contacts, :company, :string
	end	
end	