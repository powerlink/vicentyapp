class CreateContacts < ActiveRecord::Migration
  def up
    create_table :contacts do |t|
	    t.string :first_name
	    t.string :last_name
	    t.integer :user_id
	    t.integer :created_by
	    t.integer :modified_by

      t.timestamps
    end
  end

  def down
	  drop_table :contacts
  end
end
