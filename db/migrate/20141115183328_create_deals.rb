class CreateDeals < ActiveRecord::Migration
  def up
    create_table :deals do |t|
	    t.string :subject
      t.string :amount
      t.string :currency
      t.integer :company_id
	    t.integer :contact_id
      t.integer :user_id

	    t.integer :created_by
	    t.integer :modified_by
      t.timestamps
    end
  end

  def down
	  drop_table :deals
  end
end
