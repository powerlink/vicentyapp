class CreateContactsPhonesEmails < ActiveRecord::Migration
	def up
		create_table :contacts_phones_emails do |t|
			t.integer :contact_id        
			t.integer :user_id
			t.string :label
			t.string :type_code
			t.string :value
			t.integer :created_by
	    	t.integer :modified_by

      		t.timestamps

		end	
	end	

	def down
		drop_table :contacts_phones_emails
	end	
end	 