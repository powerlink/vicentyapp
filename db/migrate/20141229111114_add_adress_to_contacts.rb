class AddAdressToContacts < ActiveRecord::Migration
	def up
		#in the line bellow instead string was integer
		add_column :contacts, :adress, :string
	end	

	def down
		remove_column :contacts, :adress, :string
	end	

end	