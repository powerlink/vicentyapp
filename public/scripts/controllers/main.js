'use strict';

/**
 * @ngdoc function
 * @name vicentyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the vicentyApp
 */
angular.module('vicentyApp')
  .controller('MainCtrl', function ($scope,userService,$http) {
    userService.opCurrent('.pip');
      $scope.deals = $http.get('/contacts')
      if(!localStorage.auth_token){
			window.location.href ='#/login'
	  }
	  if(localStorage.currentUser){
			$scope.currentUser = localStorage.currentUser ;
	  }
      //userService.pipeGrigMove();
      userService.draggble();
      
      
  });
