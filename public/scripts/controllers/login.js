angular.module('vicentyApp')
  .controller('LoginController', function ($scope,userService,$http,auth,$location) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	$scope.user = {
		email:"",
		password:""
	}
	var wrong = "Email or password are incorrect, Try again";
	var notValidMail = "Email is invalid";
	var passRequired = 'password is required';
	var mailRequired = 'Email is required';
	$scope.send = function(){
		var valid = validate();
		if(!valid.mail){
			var comma = !valid.pass?",":"";
			if(valid.isEmpty){
				$scope.emailInvalid = mailRequired + comma;
			}else{
				$scope.emailInvalid = notValidMail + comma ;
			}
			$scope.CredentialsError = true;
			angular.element('input[type=text]').addClass('redBorder');
		}
		if(!valid.pass){
			$scope.passInvalid = passRequired;
			$scope.CredentialsError = true;
			angular.element('input[type=password]').addClass('redBorder');
		}


		if(valid.mail && valid.pass && !valid.isEmpty){
			$('svg').css('display','block');
			$('.formConteiner form p:last-child button').css('display','none');
			console.log(valid);
			console.log($scope.user);
			var promise = auth.login($scope.user);
			promise.then(success,error);
		}

	}

	var success = function(response){
		$('svg').css('display','none');
		$('.formConteiner form p:last-child button').css('display','block');
		console.log(response.data);
		console.log(response.data.id,"id");
		$scope.currentUser = response.data.id;
		localStorage.currentUser = response.data.id;
		localStorage.setItem('auth_token',response.data.authentication_token);
		localStorage.currentUser_name = response.data.name;
		//$location.path('/');
		window.location.href = '/';
	};
	var error = function(response){
		$('svg').css('display','none');
		$('.formConteiner form p:last-child button').css('display','block');
		$scope.wrong = wrong;
		console.log(response.config.data);
		$scope.CredentialsError = true;
		angular.element('input[type=text]').addClass('redBorder');
		angular.element('input[type=password]').addClass('redBorder');
	};

	function validate(){
		angular.element('input[type=text]').removeClass('redBorder');
		angular.element('input[type=password]').removeClass('redBorder');
		$scope.wrong = "";
		$scope.emailInvalid ="";
		$scope.passInvalid = "";
		var mail = re.test($scope.user.email);
		var isEmpty = $scope.user.email.length > 0?false:true;
		var pass = $scope.user.password.length > 5?true:false;
		return {
			mail:mail,
			pass:pass,
			isEmpty:isEmpty
		}
	}




  });
