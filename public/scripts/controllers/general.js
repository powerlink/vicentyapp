angular.module('vicentyApp').// General controller for sharing mathods and variables scopes
			controller('GeneralCtrl',function($scope,$http,userService,dba){
				// authenticate
				$scope.user_name = localStorage.currentUser_name;
				if(!localStorage.auth_token){
			  		window.location.href ='#/login'
						return;
			  	}


				// deal popup
				$scope.dealAction = false;
      			$scope.toggleDeal = function(){
        			$scope.dealAction = !$scope.dealAction;
      			}
      			$scope.$watch('dealAction', function(newValue, oldValue) {
		          if(newValue == true){
		            setTimeout(function(){
		              angular.element('.actionDealPopup').css({
		                'top':'150px',
		                'opacity':1
		              });
		            },30)
		          }else{
		            angular.element('.actionDealPopup').css({
		                'top':'-300px',
		                'opacity':0
		              });
		          }
		      	});
      			// contact popup

				

			    // activities popup
      			$scope.activitiesAction = false;
			    $scope.toggleActivities = function(){
			      $scope.activitiesAction = !$scope.activitiesAction;
			    }
				$scope.$watch('activitiesAction', function(newValue, oldValue) {
			        if(newValue == true){
			            setTimeout(function(){
			              angular.element('.actionActivitiesPopup').css({
			                'top':'100px',
			                'opacity':1
			              });
			            },30)
			        }else{
			            angular.element('.actionActivitiesPopup').css({
			                'top':'-300px',
			                'opacity':0
			              });
			        }
			    });

			    // move triangle user service
			    $scope.moveTriangle = function(event,move,releative){

					setTimeout(function(){
						var moveObj = $(move);
						var to = $(event.target);
						var widthM = moveObj.width() /2;
						var widthTo = $(event.target).width() /2;
						var offset = $(event.target).offset();
						left = offset.left;
						moveObj.css('left',left+widthTo - widthM);
					},100)
				}

				// contact post and extra
				$scope.phone_email = {}
				$scope.full_user_info = {};// reference for contact controller

				$scope.contact = {};

				$scope.deal = {};
				$scope.wrapSaveDeal = function(){
					crud.saveDeal($scope);
				}
				$scope.load_contact = function(obj,model){
					$scope.contact_deal_search = obj.contact.first_name;
					console.log($scope.contact_deal_search);
					angular.element('.dealConteinerAction .search').val(obj.contact.first_name)
					.trigger('focus');
					angular.element('.searchContactDeal').css('display','none');
					$scope.deal.contact_id = obj.contact.id;

				}
				$scope.deal_contact_obj = {};
				$scope.deal_new_contact = function(){
					crud.deal_new_contact($scope);
				}

				$scope.show_deal_contact = function(){
					angular.element('.searchContactDeal').css('display','block');
				}
				angular.element(document).on('keydown',function(e){
					if(e.keyCode == 13){
						$scope.saveContact();// to crud
					}

				})

			})
