'use strict';
angular
  .module('vicentyApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router'
  ])

    .factory('authInterceptor', function(){
       return {
         request: function(config){
           config.headers = config.headers || {};
           if (localStorage.auth_token){
             config.headers.token = localStorage.auth_token;
           }
           return  config;
         }
       }
    })

  .config (function($httpProvider){
    //$httpProvider.defaults.headers.common['Accept']='application/json';
    //$httpProvider.defaults.headers.common['Content-type']='application/json';
    //console.log($httpProvider.defaults.headers.common);
    $httpProvider.interceptors.push('authInterceptor');
  })
  .run(
    [          '$rootScope', '$state', '$stateParams',
    function ($rootScope,   $state,   $stateParams) {
      // It's very handy to add references to $state and $stateParams to the $rootScope
      // so that you can access them from any scope within your applications.For example,
      // <li ng-class="{ active: $state.includes('contacts.list') }"> will set the <li>
      // to active whenever 'contacts.list' or one of its decendents is active.
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
    }
    ]
  )
  .config(
    [          '$stateProvider', '$urlRouterProvider',
    function ($stateProvider,   $urlRouterProvider) {

      // Use $urlRouterProvider to configure any redirects (when) and invalid urls (otherwise).
      $urlRouterProvider
      // If the url is ever invalid, e.g. '/asdf', then redirect to '/' aka the home state
      .otherwise('/');
      //////////////////////////
      // State Configurations //
      //////////////////////////
      $stateProvider
      .state("login", {
        url: '/login',
        templateUrl: 'views/login.html',
        controller:'LoginController'
      })
      .state("deals", {
        url: '/',
        templateUrl: 'pipeline/pipeline.html',
        controller:'PipelineController'
      })
      .state("contacts", {

        // Use a url of "/" to set a states as the "index".
        url: '/contacts',
        templateUrl: 'contacts/contacts.html',
        controller:'ContactsController'
      })
      .state("contacts.details", {

        // Use a url of "/" to set a states as the "index".
        url: '/{contactid:[0-9]{1,4}}',
        templateUrl: 'contacts/contacts.details.html',
        controller:'ContactController'
      })
    }
    ]
  );
