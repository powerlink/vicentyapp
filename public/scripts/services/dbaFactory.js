angular.module('vicentyApp')
			.factory('dba',function($http){
				function getContacts(callback){
					$http.get('/contacts').success(callback);
				}
				var extra = function(scope,callback){
					$('.extra').each(function(index){
						var name = $('.extra').eq(index).attr('name');
						value = $('.extra').eq(index).val();

						scope.phone_email.type_code = name;
						scope.phone_email.value = value;
						scope.phone_email.label = $('.extra').eq(1).parent().children('.selectDivC').text();
						$http.post('/contacts_phones_emails',angular.copy(scope.phone_email))
							.success(callback);
					})
				}
				var saveContact = function(scope){
					scope.contact.user_id = localStorage.currentUser;
					$http.post('/contacts', scope.contact)
					.success(function(data) {
						console.log(data);
						scope.contact = {};
						setTimeout(function(){
							$('.actionContactPopup input').trigger('focusout');
						},30)
						scope.phone_email.contact_id = data.contact.id;
						scope.phone_email.user_id = data.contact.user_id;
						scope.full_user_info = data;
						scope.user_contacts.push(data);
						dba.extra(scope,function(data){
							console.log(data);
						})
						scope.toggleContact();

					});

				}
				var saveDeal = function(scope){
					scope.deal.user_id = localStorage.currentUser;
					$http.post('/deals', scope.deal)
					.success(function(data) {
						console.log(data);

					});
				}
				var deal_new_contact = function(scope){
					scope.deal_contact_obj.user_id = localStorage.currentUser;
					scope.deal_contact_obj.first_name = scope.contact_deal_search;
					$http.post('/contacts',scope.deal_contact_obj).success(function(data){
						console.log(data);
						 scope.deal.contact_id = data.contact.id;
					})
					angular.element('.searchContactDeal').css('display','none');
				}
				var dba = {
					getContacts:getContacts,
					extra:extra,
					saveContact:saveContact,
					saveDeal:saveDeal,
					deal_new_contact:deal_new_contact
				}

				return dba;
			})
