angular.module('vicentyApp')
.factory('userService', function(){
    function responsive(sumId,staticNum){
		var sum = angular.element(sumId).width();
		var left = staticNum / sum * 100;
		var right = 100 - left;
		if(arguments[2]){
			for(i=2;i<=arguments.length;i++){
				angular.element(arguments[i]).width(right+'%');
			}
		}
		
	}
	function opCurrent(target){
		setTimeout(function(){//set timeout need to be removed
			angular.element('.options').removeClass('option-current');
			angular.element(target).addClass('option-current');
		},60)
				 
	}
	

	function draggble(){
			$('.drag').mousedown(function(){
				$(window).unbind('mousewheel');
				$(this).children('.piplineInfo').css({
					'border':'1px solid #d0d2d3'
				});
				$('.addDeal').css('opacity','0');
				$('#status').show();


			})


			$('.drag').mouseup(function(){

				
				that = $(this);
				$(this).children('.piplineInfo').css({
					'border':'none',
					'border-bottom':'1px solid #d0d2d3',
					'z-index':'99999999999999999999999'
				});
				$('#status').hide();
	       		$('.addDeal').css('opacity','1')	   
				
			})
			$( ".peeplinedragConteiner" ).sortable({
			  appendTo: 'body',
    		  scroll: false, //stops scrolling container when moved outside boundaries	
		      connectWith: ".connect",
		      placeholder: "ui-state-highlight",
		      start: function( event, ui ) {
		      
		      },
		      stop:function(){

		      	$('.drag').trigger('mouseup');

		      	that.children('.piplineInfo').css({
					'border':'none',
					'border-bottom':'1px solid #d0d2d3',
					'z-index':'99'
					
				});
		      }
		    }).disableSelection();
	}


	function toogleBackground(){
		var $all = $('.userContact > div:nth-child(2)> div > div').not('.contactStep,.contactDate,.contactOwner');
		var id = $(this).attr('id');
		$all.click(function(){
			$all.each(function(){
				var src = $(this).css('background-image');
				src = src.replace('blue','grey');
				$(this).css('background-image',src);
				$(this).children('h3').css('color','#929497');
			})
			var src = $(this).css('background-image');
				src = src.replace('grey','blue');
				$(this).css('background-image',src);
				$(this).children('h3').css('color','#5E99E7');
		})
	}

	function moveTriangle(event,move,releative){
			    	
					setTimeout(function(){
						var moveObj = $(move);
						var to = $(event.target);
						var widthM = moveObj.width() /2;
						var widthTo = $(event.target).width() /2;
						var offset = $(event.target).offset();
						left = offset.left;
						moveObj.css('left',left+widthTo - widthM);
					},100)
					
				
				}
	
    var fac = {
			responsive:responsive,
			opCurrent:opCurrent,
			draggble:draggble,
			toogleBackground:toogleBackground
			
    };

     
    
     
    return fac;
 
});