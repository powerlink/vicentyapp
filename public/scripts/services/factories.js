angular.module('vicentyApp').factory('Contact', function($resource) {
  return $resource('/contacts/:id'); // Note the full endpoint address
});
