
angular.module('vicentyApp')

  .controller('ContactController', function ($scope,userService,$http,dba,$stateParams,Contact) {


    $scope.contact = Contact.get({ id: $stateParams.contactid})

    $scope.wrapSaveContact = function(){
      dba.saveContact($scope);
    }
    $scope.contactAction = false;
    $scope.toggleContact = function(){
      // if($scope.contact == true){
      // 	$('.actionContactPopup').css({'top':'-460px','opacity':'0'})
      // }
      $scope.contactAction = !$scope.contactAction;
    }
    $scope.$watch('contactAction', function(newValue, oldValue) {
      if(newValue == true){
        setTimeout(function(){
          angular.element('.actionContactPopup').css({
            'top':'100px',
            'opacity':1
          });
        },30)
      }else{
        angular.element('.actionContactPopup').css({
          'top':'-300px',
          'opacity':0
        });
      }
    });
    /****** toogle scope *********
    $scope.ngUrl;
    $scope.dinamicUrl = function(url){
      var path = 'views/partials/';
      var ext = '.html'
      var integrateUrl = path + url + ext;
      $scope.ngUrl = integrateUrl;
    }
    $scope.dinamicUrl('dealsUserContact');
    // contact popup

  /******************** http calls  ************************/
    $scope.show_full_user_info = function(user_contact){
      $scope.full_user_info = user_contact;
      console.log($scope.full_user_info);

    }
   /******************** user service angular seprate javascript ui *********
    angular.element(document).ready(function(){

      angular.element('span,h1,p,h2,h3').not('.header .search span').addClass('text');
      userService.responsive('.deal-card',338,'.dealCard-right');
      userService.responsive('.contact',338,'.addDealC','.userContact');
      userService.toogleBackground();
    })
    angular.element(window).resize(function(){

      userService.responsive('.contact',338,'.addDealC','.userContact');
      userService.responsive('.deal-card',338,'.dealCard-right');
    })
    */
  });
