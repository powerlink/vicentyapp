class ContactsController < ApplicationController
		before_filter :authenticate_user_from_token!


	def index
		@contacts = Contact.all
		render 'index'
	end

	def show
		@contact = Contact.find_by_id params[:id]
		if @contact
			render 'show', :status => :ok
		else
			set_errors(@contact, :contact)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def create
		@contact = Contact.new(create_params)
		if @contact.save
			render 'show', :status => :created
		else
			set_errors(@contact, :contact)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def update
		@contact = Contact.find_by_id(params[:id])
		if @contact && @contact.update_attributes(update_params)
			render 'show', :status => :ok
		else
			set_errors(@contact, :contact)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def delete
		@contact = Contact.find_by_id(params[:id])
		if @contact && @contact.destroy
			head :no_content, :status => :ok
		else
			set_errors(@contact, :contact)
			render 'error_json', :status => :unprocessable_entity
		end
	end


	private

	def create_params
		c_params = params.permit(:first_name, :last_name, :user_id, :job_title, :email, :phone, :adress, :company)
		c_params.merge({ :created_by => current_user.id, :modified_by => current_user.id })
	end

	def update_params
		u_params = params.permit(:first_name, :last_name, :user_id, :job_title, :email, :phone, :adress, :company)
		u_params.merge({ :modified_by => current_user.id })
	end

end
