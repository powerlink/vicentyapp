class DealsController < ApplicationController
		before_filter :authenticate_user_from_token!


	def index
		@deals = Deal.all
		render 'index'
	end

	def show
		@deal = Deal.find_by_id params[:id]
		if @deal
			render 'show', :status => :ok
		else
			set_errors(@deal, :deal)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def create
		@deal = Deal.new(create_params)
		if @deal.save
			render 'show', :status => :created
		else
			set_errors(@deal, :deal)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def update
		@deal = Deal.find_by_id(params[:id])
		if @deal && @deal.update_attributes(update_params)
			render 'show', :status => :ok
		else
			set_errors(@deal, :deal)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def delete
		@deal = Deal.find_by_id(params[:id])
		if @deal && @deal.destroy
			head :no_content, :status => :ok
		else
			set_errors(@deal, :deal)
			render 'error_json', :status => :unprocessable_entity
		end
	end


	private

	def create_params
		c_params = params.permit(:currency, :amount, :subject, :contact_id)
		c_params.merge({ :created_by => current_user.id, :modified_by => current_user.id })
	end

	def update_params
		u_params = params.permit(:currency, :amount, :subject, :contact_id)
		u_params.merge({ :modified_by => current_user.id })
	end

end