class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session


  def set_errors(obj, obj_name)
	  @errors = []
	  @errors << "Invalid #{obj_name}" and return if !defined?(obj) or obj.nil?
	  @errors << 'Unknown error' and return if obj.errors.blank?
	  @errors = obj.errors.full_messages
  end
  def authenticate_user_from_token!
    token = request.headers['token'].presence
    user = token && User.find_by_authentication_token(token.to_s)
    if user
      sign_in user, store: false
    else
			render :json => {}, status: :unauthorized
    end
  end
end
