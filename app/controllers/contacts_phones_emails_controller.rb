class ContactsPhonesEmailsController < ApplicationController
	before_filter :authenticate_user_from_token!


	def index
		@phones_emails = ContactsPhonesEmails.all
		render 'index'
	end

	def show
		@phone_email = ContactsPhonesEmails.find_by_id params[:id]
		if @phone_email
			render 'show', :status => :ok
		else
			set_errors(@phone_email, :contacts_phones_emails)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def create
		@phone_email = ContactsPhonesEmails.new(create_params)
		if @phone_email.save
			render 'show', :status => :created
		else
			set_errors(@phone_email, :contacts_phones_emails)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def update
		@phone_email = ContactsPhonesEmails.find_by_id(params[:id])
		if @phone_email && @phone_email.update_attributes(update_params)
			render 'show', :status => :ok
		else
			set_errors(@phone_email, :contacts_phones_emails)
			render 'error_json', :status => :unprocessable_entity
		end
	end

	def delete
		@phone_email = ContactsPhonesEmails.find_by_id(params[:id])
		if @phone_email && @phone_email.destroy
			head :no_content, :status => :ok
		else
			set_errors(@phone_email, :contacts_phones_emails)
			render 'error_json', :status => :unprocessable_entity
		end
	end


	private

	def create_params
		c_params = params.permit(:value, :type_code, :contact_id, :user_id, :label)
		c_params.merge({ :created_by => current_user.id, :modified_by => current_user.id })
	end

	def update_params
		u_params = params.permit(:value, :type_code, :contact_id, :user_id, :label)
		u_params.merge({ :modified_by => current_user.id })
	end
end
